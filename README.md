# Các nội dung tìm hiểu
## 1. [Tìm hiểu cơ bản về Linux](./Training VTN/Hệ_điều_hành_LINUX.md)
## 2. [Tìm hiểu kiến thức về Markdown](./Training VTN/Markdown.md)
## 3. [Tìm hiểu kiến thức về Git](./Training VTN/Git.md)
## 4. [Tìm hiểu về Virtualization](./Training VTN/Tìm_hiểu_về_Virtualization.md)
## 5. [Tìm hiểu kiến thức về  Container](./Training VTN/Tìm_hiểu_về_Container.md)
## 6. [Tìm hiểu kiến thức về  DOCKER](./Training VTN/DOCKER CONTAINER.md)
## 7. [Tìm hiểu kiến thức về  Ansible](./Training VTN/Ansible Overview.md)

# DDoS attack detection using SVM in SDN
## [Project DDoS using SVM](./DDoS using SVM)
