## Tìm hiểu về Containerzation

### I. Container basics
**1. Container là gì,  các loại container**

**Containers** là công nghệ ảo hóa hệ điều hành được sử dụng để gói applications và các phụ thuộc của chúng (dependencies), chạy trong các môi trường cô lập. Chúng cung cấp phương pháp nhẹ nhàng để gói và triển khai dcapplications theo cách tiêu chuẩn hóa trên nhiều loại hạ tầng khác nhau.

**Cách hoạt động của Container**
-  Container ảo hóa hệ điều hành một cách trực tiếp thay vì ảo hóa toàn bộ máy tính. Chúng chạy như quy trình chuyên biệt được quản lý bởi kernel của hệ điều hành máy chủ, nhưng với một cảnh nhìn bị ràng buộc và bị điều khiển nặng nề về quy trình, tài nguyên và môi trường của hệ thống. Containers không biết rằng chúng tồn tại trên một hệ thống dùng chung và hoạt động như thể chúng có toàn quyền kiểm soát máy tính.
- Thay vì xử lý containers như thể chúng là máy tính đầy đủ như máy ảo, thì việc quản lý containers tương tự như applications là phổ biến hơn. Chẳng hạn, tuy có thể gắn SSH server vào một container nhưng nó không phải là mô hình được đề xuất. Thay vào đó, việc gỡ lỗi thường được thực hiện thông qua giao diện ghi nhật ký, các cập nhật được áp dụng bằng cách cuộn các images mới và quản lý service được nhấn mạnh để ưu tiên quản lý toàn bộ container.
- Những đặc điểm này có nghĩa là containers chiếm một không gian ở đâu đó giữa các máy ảo được cô lập và quản lý riêng của các quy trình thông thường. Containers cung cấp khả năng phân vùng và ảo hóa tập trung vào quá trình, cung cấp sự cân bằng tốt về sự giới hạn, tính linh hoạt và tốc độ.

**Lợi ích**
- Tiết kiệm tài nguyên: Các container đòi hỏi ít tài nguyên hơn môi trường máy ảo hay phần cứng truyền thống vì chúng không chứa hệ điều hành.
- Tăng tính linh hoạt: Các ứng dụng chạy trong container có thể được triển khai trên nhiều hệ điều hành và nền tảng phần cứng khác nhau một cách dễ dàng.
- Vận hành nhất quán: Các nhóm DevOps team có thể yên tâm bởi các ứng dụng trong container sẽ chạy hoàn toàn tương tự, bất kể chúng được triển khai ở đâu.
- Hiệu quả cao: Các container cho phép các ứng dụng được triển khai, vá hoặc thu nhỏ với thời gian nhanh hơn.
- Phát triển ứng dụng tốt hơn: Các container cũng hỗ trợ các DevOps trong việc tăng tốc các chu kỳ phát triển, thử nghiệm và sản xuất ứng dụng.
**Một số phương pháo ảo hóa Container:**

+ OpenVZ: Ban đầu dự án OpenVZ nằm trong giải pháp ảo hoá server gọi là Virtuozzo, được thực hiện bởi công ty SWsoft năm 1997. Năm 2005, một phần sản phẩm của Virtuozzo được công bố như một dự án mã nguồn mở, sau này là OpenVZ. Sau đó, năm 2008 SWsoft sáp nhập với công ty Parallels. OpenVZ được sử dụng cho hosting và các dịch vụ cloud, và được gọi là Parallels Cloud Server. OpenVZ dựa trên Linux kernel đã được sửa lại. Command-line tools (primarily vzctl) được sử dụng để tạo các container Linux.

+ Google containers: năm 2013, Google cho ra mắt bản mã nguồn mở của container stack, gọi là lmctfy (Let Me Contain That For You). Hiện tại thì nó vẫn đang ở bản Beta. Hầu như mọi sản phẩm của Google đều sử dụng container.

+ Linux-VServer: một dự án mả nguồn mở, được ra mắt năm 2001. Nó cung cấp các phân vùng tài nguyên trên host và host này phải sửa dụng kernel đả được chỉnh sửa.

+ LXC: dựa án LXC (LinuX Containers) cung cấp một bộ gồm công cụ hữu ích để quản lý Linux container. Nhiều người đóng góp trong dự án đến từ team OpenVZ. Ngược lại với OpenVZ thì nó được chạy trên kernel ko chỉnh sửa. LXC được viết hoàn chỉnh trong userspace và hỗ trợ liên kết với các ngôn ngữ khác như Python, Lua hay Go. Nó chạy được trên hầu hết các phiên bản của Linux như Fedora, Ubuntu, Debian



**2. Containerzation vs Virtualization**

**So sánh**

|**Area**|**Virtualization**|**Containerization**|
| --- | --- | --- |
|Isolation|Tách biệt giữa Host OS và các máy ảo khác với nhau|Tách biệt giữa Host và VM, nhưng không cung cấp ranh giới bảo mật mạnh mẽ như máy ảo|
|Operating System|Chạy một hệ điều hành hoàn chỉnh bao gồm hạt nhân, do đó yêu cầu nhiều tài nguyên hệ thống hơn như CPU, bộ nhớ và lưu trữ|Chạy phần chế độ người dùng của hệ điều hành và có thể được điều chỉnh để chỉ chứa các dịch vụ cần thiết cho ứng dụng của bạn bằng cách sử dụng ít tài nguyên hệ thống hơn|
|Khả năng tương thích |Chạy bất kỳ hệ điều hành nào bên trong máy ảo|Chạy trên cùng phiên bản hệ điều hành với máy chủ|
|Deployment|Triển khai các máy ảo riêng lẻ bằng cách sử dụng phần mềm Hypervisor|Triển khai các bộ chứa riêng lẻ bằng cách sử dụng Docker hoặc triển khai nhiều bộ chứa bằng cách sử dụng điều phối như Kubernetes |
|Pesistent storage|Sử dụng Virtual Hard Disk (VHD) để lưu trữ cục bộ cho một máy ảo hoặc chia sẻ  Server Message Block (SMB) để lưu trữ được chia sẻ bởi nhiều máy chủ|Sử dụng đĩa cục bộ để lưu trữ cục bộ cho một nút hoặc SMB để lưu trữ được chia sẻ bởi nhiều nút hoặc máy chủ|
|Load balancing|Được thực hiện bằng cách chạy máy ảo trong các máy chủ khác trong cụm chuyển đổi dự phòng|Bộ điều phối có thể tự động khởi động hoặc dừng bộ chứa trên các nút cụm để quản lý các thay đổi về tải và tính khả dụng.|
|Networking|Sử dụng Virtual network adapters|Sử dụng chế độ Isolated của Virtual network adapter. Do đó, cung cấp ít tính năng ảo hóa hơn một chút|.

**Virtualization**
Ưu điểm:
- Có thể chạy nhiều HĐH cùng một lúc
- Dễ dàng khôi phục và bảo trì trong trường hợp bị lỗi.

![lỗi](./Image in VM/C1.png)

Nhược điểm:
- Hiệu suất không ổn định do chạy nhiều máy ảo.
- Hiệu quả của Hypervisors không cao bằng HĐH của máy chủ.
- Thời gian khởi động lâu.

**Containerzation**
Ảo hóa cấp độ hệ điều hành, Containerzation hiệu quả hơn vì không có Guest OS để sử dụng host's OS và không phải chia sẻ các thư viện và tài nguyên. 

![lỗi](./Image in VM/C2.png)

Sơ đồ trên cho thấy một host OS được chia sẻ bởi tất cả Container. Container chỉ chứa các ứng dụng dành riêng cho thư viện được isolated với các container khác để không lãng phí tài nguyên. Điều này làm cho việc triển khai Container nhanh và đáng tin cậy hơn.


### II. Nguồn tham khảo

[Tìm hiểu về Containers | CloudFun](https://cloudfun.vn/threads/tim-hieu-ve-containers.318/)

[Virtualization vs Containerization | Baeldung on Computer Science](https://www.baeldung.com/cs/virtualization-vs-containerization)

[Container-based virtualization (viblo.asia)](https://viblo.asia/p/container-based-virtualization-eBYjv4K1GxpV)

[Containerization vs Virtualization | Everything you need to know](https://www.cloudmanagementinsider.com/containerization-vs-virtualization/)