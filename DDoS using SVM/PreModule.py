import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split

from sklearn.svm import SVC

from sklearn.metrics import accuracy_score
from sklearn.model_selection import cross_val_score

# Import the dataset
df = np.loadtxt(open('data/Data.csv','rb'), delimiter=',')

# Splitting dataset into features and label
X = df[:, 0:5]
y = df[:, 5]

# Splitting the dataset into the training set and the test set Test-size is 0.25(25%)
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.25, random_state=0)


# Fitting SVM with the training set
classifier = SVC(kernel='linear', C=0.025)
flow_model = classifier.fit(X_train, y_train)

# Testing the model by classifying the test set
y_pred = classifier.predict(X_test)
#Calculate the accuracy
acc = accuracy_score(y_test, y_pred)
print ("Succes accuracy =", acc*100,'%')
fail = 1.0 - acc
print ("Fail accurace =",fail*100,'%')
