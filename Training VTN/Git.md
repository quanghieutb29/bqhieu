### Tìm hiểu về Git
**1. Git**

**a. Tổng quan**

**Git** là một hệ thống quản lý phiên bản phân tán (Distributed Version Control System – DVCS), nó là một trong những hệ thống quản lý phiên bản phân tán phổ biến nhất hiện nay. Git cung cấp cho mỗi lập trình viên kho lưu trữ (repository) riêng chứa toàn bộ lịch sử thay đổi.

- Trên Git, ta có thể lưu trạng thái của file dưới dạng lịch sử cập nhật. Vì thế, có thể đưa file đã chỉnh sửa một lần về trạng thái cũ hay có thể biết được file đã được chỉnh sửa chỗ nào.

- Git sẽ giúp: 

\+ Lưu lại được các phiên bản khác nhau của mã nguồn dự án phần mềm.

\+ Khôi phục lại mã nguồn từ một phiên bản bất kỳ.

\+ Dễ dàng so sánh giữa các phiên bản.

\+ Phát hiện được ai đã sửa phần nào làm phát sinh lỗi.

\+ Khôi phục lại tập tin bị mất.

\+ Dễ dàng thử nghiệm, mở rộng tính năng của dự án mà không làm ảnh hưởng 	đến phiên bản chính (master branch).

\+ Giúp phối hợp thực hiện dự án trong nhóm 1 cách hiệu quả.

**b.** **Các khái niệm cơ bản:**

**Respository ( Kho lưu trữ):** là nơi lưu trữ, quản lý tất cả những thông tin cần thiết (thư mục, tập tin, ảnh, video, bảng biểu, dữ liệu… ) cũng như các sửa đổi và lịch sử của toàn bộ dự án. Khi tạo mới repository, bạn nên tạo thêm tập tin README hoặc một tập tin thông tin giới thiệu về dự án của bạn. 

**Object store:** nó chứa dữ liệu nguyên gốc (original data files), các file log ghi chép quá trình sửa đổi, tên người tạo file, ngày tháng và các thông tin khác.

**Index:** giữa repository và working tree tồn tại một nơi gọi là index. Index là nơi để chuẩn bị cho việc commit lên repository.

**Branch (nhánh):** Mỗi nhánh trong Git gần giống như một workspace. Nhánh (branch) được dùng để phát triển tính năng mới mà không làm ảnh hưởng đến code hiện tại. Nhánh master là nhánh “mặc định” khi bạn tạo một repository. Nhánh master thông thường là nhánh chính của ứng dụng. 

**Git work-flow:** là 1 chu trình dùng để thực hành cũng như sử dụng Git. Ví dụ như là phương pháp tạo loại branch nào, những branch nào nên merge với nhau...

**Staging Area**: là khu vực sẽ lưu trữ những thay đổi của bạn trên tập tin để nó có thể được commit, vì muốn commit tập tin nào thì tập tin đó phải nằm trong Staging Area. Một tập tin khi nằm trong Staging Area sẽ có trạng thái là Stagged.

**c. Git, Github, Gitlab giống và khác nhau như thế nào?**

**Git** là một hệ thống kiểm soát phiên bản phân tán mã nguồn mở có sẵn cho tất cả mọi người với chi phí bằng không. Nó được thiết kế để xử lý các dự án từ nhỏ đến lớn với tốc độ và hiệu quả. Nó được phát triển để điều phối công việc giữa các lập trình viên. Kiểm soát phiên bản cho phép bạn theo dõi và làm việc cùng với thành viên trong nhóm của mình tại cùng một không gian làm việc.

GitHub cũng là một nền tảng lưu trữ online lớn về các dự án nhiều người làm.

GitLab là hệ thống self-hosted mã nguồn mở dựa trên hệ thống máy chủ Git dùng để quản lý mã nguồn của bạn. GitLab cung cấp giải pháp server một cách hoàn hảo

**Phân biệt :**

- Git là một công cụ kiểm soát phiên bản phân tán có thể quản lý lịch sử mã nguồn còn GitHub và GitLab là một công cụ dựa trên đám mây được phát triển xung quanh công cụ Git.
- Git là 1 công cụ cục bộ còn GitHub và GitLab là một dịch vụ trực tuyến để lưu trữ code và đẩy từ máy tính chạy công cụ Git.
- Git tập trung vào kiểm soát phiên bản và chia sẻ code còn GitHub và GitLab tập trung vào lưu trữ code
- Git là 1 công cụ dòng lệnh còn GitHub và GitLab được quản lí thông qua trang web.
- Git không cung cấp bất kỳ tính năng quản lý người dùng nào còn GitHub và GitLab được tích hợp sẵn tính năng quản lý người dùng.

**2. Config cho git với git config**
- Config thông tin cá nhân
- Config default/prefered editor
- Config global .gitignore

***Các lệnh cấu hình***

$ git config --global user.name “”

$ git config --global user.email “”

![lỗi](./Image in Git/Aspose.Words.e2de9178-e454-4ec6-a6be-84805dadab27.001.png)


**3. Các thao tác ban đầu**

**Khởi tạo repo với git init**

Lệnh git init được sử dưng để tạo, khởi tạo một kho chứa Git mới (Git Repo) ở local. Khi đang trong thư mục dự án chạy lệnh git init nó sẽ tạo ra một thư mục con (ẩn) tên .git, thư mục này chứa tất cả thông tin mô tả cho kho chứa dự án (Repo) mới - những thông tin này gọi là metadata gồm các thư mục như objects, refs ... Có một file tên HEAD cũng được tạo ra - nó trỏ đến commit hiện tại.



**Lệnh:** $ git init <tên kho chứa>

![lỗi](./Image in Git/Aspose.Words.e2de9178-e454-4ec6-a6be-84805dadab27.002.png)

**Clone repo với git repo**

Lệnh git clone để sao chép, copy một Git Repo (kho chứa dự án Git) về máy đang local. Một số trường hợp sử dụng git clone như:

\+ Copy một Repo từ máy Remote về Local

\+ Copy một Repo từ thư mục này sang một thư mục khác

\+ Copy một Repo từ một Url (https) ví dụ GitHub

**Lệnh:** $git clone <thư mục/url>

**4. Git commit**

**Commit** là thao tác báo cho hệ thống biết bạn muốn lưu lại trạng thái hiện hành, ghi nhận lại lịch sử các xử lý như đã thêm, xóa, cập nhật các file hay thư mục nào đó trên repository. 
Khi thực hiện commit, trong repository sẽ ghi lại sự khác biệt từ lần commit trước với trạng thái hiện tại. Các commit ghi nối tiếp với nhau theo thứ tự thời gian do đó chỉ cần theo vết các commit thì có thể biết được lịch sử thay đổi trong quá khứ.
Một commit chứa tất cả những thay đổi của mình tất cả những gì mình đã làm và có ngày giờ cụ thể tại thời điểm chúng ta thay đổi.
Git nhằm mục đích giữ cho cam kết càng nhẹ càng tốt. Vì vậy, nó không sao chép một cách mù quáng toàn bộ thư mục mỗi khi bạn commit; nó lưu trữ bao gồm các commit như một tập hợp các thay đổi hoặc nó chuyển phiên bản của kho lưu trữ từ phiên bản này sang phiên bản khác mới hơn. Nói một cách dễ hiểu, nó chỉ sao chép những thay đổi được thực hiện trong kho lưu trữ. Bạn có thể commit bằng cách sử dụng lệnh:
$ git commit

**hoặc**  $ git commit –m <nội dung thông điệp cần lưu trữ>

**Git – amend**: Trong một số trường hợp bạn commit nhưng bị quên add một số file nào đó và bạn không muốn tạo ra một commit mới thì có thể sử dụng lệnh commit kết hợp tham số --amend để gộp các file đó và bổ sung vào commit cuối cùng, vì vậy không tạo ra commit mới.  
Lệnh: $ git commit –amend

Nếu thực hiện lệnh này ngay sau commit cuối cùng mà không thay đổi gì thêm thì ảnh của commit cuối cùng đó không thay đổi gì, chỉ là nội dung message thay đổi mà thôi. Trình soạn thảo sẽ hiển thị để bạn thay đổi message, đương nhiên bạn có thể để message mặc định mà git đã tạo ra, và message này sẽ ghi đè lên message trước đó.

VD:
$ git commit -m "Bo sung header"

$ git add "newfile.html"

$ git commit –amend

Như vậy để bổ sung vào commit cuối cùng thì ta sử dụng lệnh commit nhưng thêm tham số -amend vào phía sau $ git commit --amend.


**5. Làm việc với Branch**

**Branch** là những phân nhánh ghi lại luồng thay đổi của lịch sử, các hoạt động trên mỗi branch sẽ không ảnh hưởng lên các branch khác nên có thể tiến hành nhiều thay đổi đồng thời trên một repo, giúp giải quyết được nhiều nhiệm vụ cùng lúc.

Khi tham gia dự án với mỗi nhiệm vụ chúng ta sẽ tạo một branch và làm việc trên đó, các branch này sẽ hoạt động riêng lẻ và không ảnh hưởng lẫn nhau.

**Tạo mới branch**

Để tạo 1 branch mới ta sử dụng lệnh : $ git branch <name>

**Nhảy từ branch này sang branch khác**

Khi nhảy từ branch này sang branch khác ta dùng câu lệnh: 

$ git checkout <branch\_name>

**Đổi tên branch**

Muốn đổi tên branch ta dùng lệnh: $ git branch –m <old\_name> <new\_name>

**6. Git checkout**

Lệnh git checkout được dùng để chuyển nhánh hoặc để phục hồi file trong thư mục làm việc từ một commit trước đây .

**Lệnh:** $ git checkout <branch>

VD ta đang ở nhánh nào đó muốn chuyển sang nhánh master thì thực hiện lệnh: 

$ git checkout master 

Lúc này nhánh master hoạt động, và thư mục làm việc là các file tương ứng với nhánh này.

**7. Các thao tác liên quan khác**

**Git Merge**

Git Merge là một lệnh dùng để hợp nhất các chi nhánh độc lập thành một nhánh duy nhất trong Git.

Khi sử dụng lệnh hợp nhất trong Git, chỉ có nhánh hiện tại được cập nhật để phản ánh sự hợp nhất, còn nhánh đích sẽ không bị ảnh hưởng.

Để merge một branch bất kì vào branch hiện tại thì bạn sử dụng lệnh: 

$ git merge <branch\_name>

**Git Rebase**

Git Rebase là một chức năng được dùng khi gắn nhánh đã hoàn thành công việc vào nhánh gốc . Việc điều chỉnh nhánh công việc gắn vào với nhánh gốc nên các commit sẽ được đăng kí theo thứ tự gắn vào .

Câu lệnh : $ git rebase <branch\_name>

**Git Rebase –interactive**

Khi chúng ta commit thiếu file lên git server hoặc ghi nội dung comment chưa được như ý muốn hoặc nội dung comment chưa đúng theo quy tắc chung đã đưa ra.

Lệnh: $ git rebase –i <base>

**Git Pull**

Git Pull là một lệnh dùng để tải xuống dữ liệu từ một Remote repo và cập nhật Local repo phù hợp với dữ liệu đó. Có nghĩa là Git Pull là lệnh hợp nhất các thay đổi từ Remote repo vào Local repo.

**Git Push**

Lệnh git push được sử dụng để đẩy các commit mới ở local repo lên remote repo. Nguồn để đẩy lên là nhánh mà con trỏ HEAD đang trỏ tới (nhánh làm việc).

Lệnh git push cũng có thể xóa đi các nhánh của remote.

Một số tham số hay dùng như:

\+ All đẩy tất cả các nhánh lên server

\+ Tags đẩy tất cả các tag lên server

\+ Delete xóa một nhánh chỉ ra trên server

\+ Đẩy và tạo một upstream (luồng upload tương ứng với nhánh của local), hay 	   sử dụng cho lần đầu đẩy lên server

- **Đẩy lên server lần đầu tiên**

Nếu là lần đầu tiên đẩy Local Repo lên Remote Repo mới khởi tạo thì cần tạo ra một theo dõi kết nối, upstream giữa local và remote, vậy hãy dùng tham số -u. Ví dụ đẩy lên remote có tên origin và tạo upstream cho nhánh master

Lệnh : $ git push -u origin master

- Đẩy lên server

Sau khi có upstream, mỗi lần cần đẩy dữ liệu lên remote của nhánh master, chỉ việc thực hiện lệnh

Lệnh : $ git push

- Đẩy lên server tất cả các nhánh

Đẩy tất cả các nhánh ở local lên server có tên origin:

Lệnh : $ git push origin –all

- Xóa một nhánh trên remote

VD xóa nhánh test, trên remote có tên origin

Lệnh : $ git push origin --delete test

- Ghi đè nhánh với --force

Có thể ghi đè toàn bộ một nhánh ở remote bởi một nhánh ở master

VD ghi đè toàn nhánh master ở remote, giống với master của local

Lệnh : $ git push --force origin master

**Git Cherry-pick**

Là một cách để checkout 1 commit bất kỳ tại 1 branch được chỉ định về brach hiện tại. Hay chính là git cherry-pick sẽ lấy thay đổi của 1 commit trên 1 nhánh nào đó áp dụng vào nhánh hiện tại.

**Tùy chọn force**

Lệnh : git push -f origin master cho phép push kho lưu trữ cục bộ đến từ xa mà không cần giải quyết xung đột. Cách này khá nguy hiểm vì theo cơ chế, nó sẽ ghi đè lên remote repo bằng code ở local của mình, mà không cần quan tâm đến việc bên phía remote đang chứa thứ gì. Cách push an toàn : sử dụng push --force-with-lease, git sẽ từ chối việc update lên branch trừ khi branch đó nằm trong trạng thái "được coi là an toàn".

**Git reset**

Git reset được dùng để quay về một điểm commit nào đó, đồng thời xóa lịch sử của các commit trước nó.

Sử dụng reset để xóa commit : $ git reset --hard HEAD


**8. Thao tác với git remote**

**Add remote**

Để thêm một remote repo vào local repo thì bạn sử lệnh: $ git remote add [shortname] [url]

Trong đó:

 \+ shortname: là tên mà bạn muốn đặt cho remote repo

 \+ url: là đường dẫn trỏ đến repo, phần đuôi của URL sẽ là .git

**Rename remote**

Để đổi tên cho remote thì bạn sử dụng lệnh sau :

$ git remote rename <old\_name> <new\_name>

**Update remote**

Để cập nhật dữ liệu từ remote repo về local repo thì ta sử dụng lệnh git fetch hoặc lệnh git pull.

Lệnh git fetch tải về dữ liệu từ Remote Repo (các dữ liệu như các commit, các file, refs), dữ liệu tải về để chúng ta theo dõi sự thay đổi của remote, tuy nhiên tải về bằng git fetch nó chưa tích hợp thay đổi ngay local repository, mục đích để theo dõi các commit người khác đã cập nhật lên server, để có được thông thông tin khác nhau giữa remote và local.

Lệnh git pull lấy về thông tin từ remote và cập nhật vào các nhánh của local repo.

**Prune remote branch**

Lệnh : $ git remote prune

Xóa các ref cho các nhánh không tồn tại trên remote.

Nó loại bỏ các nhánh từ xa không có đối tác cục bộ. Ví dụ : nếu có một nhánh từ xa nói bản demo, nếu nhánh này không tồn tại cục bộ, thì nó sẽ bị xóa.

**9. Git Stash**

**Git stash** là lệnh dùng để thiết lập trạng thái ban đầu cho tất cả các file nằm trong

thư mục làm việc (working directory). Trạng thái ban đầu ở đây chính là nội dung

dữ liệu ban đầu của file.

Lệnh git stash sẽ có tác dụng với tất cả dữ liệu đang hoạt động trong working

directory với điều kiện là dữ liệu đó đã được đưa vào trạng thái Staged hoặc đã từng

được committed.

**git stash list**

Xem danh sách bao nhiêu lệnh stash đã dùng.

Nếu chúng ta đã stash rất nhiều lần và muốn xem danh sách và địa chỉ của nó thì sử dụng  lệnh : $ git stash list

**git stash clear**

Nếu chúng ta muốn xóa tất cả stash ra khỏi history thì sử dụng tham số clear 

Lệnh : $ git stash clear

- Lúc này danh sách stash sẽ rỗng.


