### Hệ điều hành LINUX

**1. Tổng quan về Linux:**
- **Linux** là hệ điều hành máy tính được phát triển bởi nhà khoa học máy tính Linus Torvalds dựa trên hệ điều hành Unix. Linux là hệ điều hành miễn phí dành cho người dùng và được phát hành dưới dạng phần mềm mã nguồn mở cho phép nhà phát triển được tự do sửa đổi và bổ sung source code của hệ điều hành này...
- **Linux distribution: (Distro)** là một tập hợp các ứng dụng, các gói ứng dụng (package), trình quản lý gói và các tính năng chạy trên nhân Linux, như vậy nhân Linux là dùng chung cho các bản phân phối – có đôi khi nhân này được tùy chỉnh lại theo tổ chức bảo trì bản phân phối
- **Chọn bản phân phối Linux**  theo 3 tiêu chí:

**+ Mục đích sử dụng:** Mỗi distro được thiết kế cho các mục đích khác nhau, cung cấp trải nghiệm người dùng khác nhau. Một số distro dùng để làm server, một số lại dùng cho môi trường desktop, một số lại có mục đích đặc biệt như hệ thống nhúng. Cho đến nay, các bản Linux được cài đặt nhiều nhất và là để làm các server để chạy các dịch vụ (như website), còn các bản Linux cài đặt tạo môi trường desktop (máy tính cá nhân) vẫn là rất ít nếu so với Windows hay macOS.

**+ Cấu hình và gói ứng dụng:** Các bản phân phối còn có sự khác nhau chính đó là cách thức thiết lập cấu hình hệ thống của chúng khác nhau. Một số Distro giữ các cấu hình, thiết lập, các file cấu hình ở cùng một nơi (thư mục), một số khác lại lưu ở nhiều nơi trong cấu trúc thư mục. Tiếp theo là quá trình cài đặt, cập nhật các ứng dụng (các gói package) cũng khác nhau tùy vào distro, nhiều distro thực hiện điều này bằng các công cụ quản lý gói (package) như DPKG (debian), APT (ubuntu, debian), RPM (Red Hat), YUM … Do có nhiều trình quản lý gói kiểu này nên thật sự việc quản trị gây khó khi làm việc giữa nhiều distro, ở các bài sau sẽ nói về những công công cụ quản lý gói này.

**+ Mô hình hỗ trợ:** Một số distro được bảo trì, hỗ trợ bởi cộng đồng tình nguyện (Debian, CentOS, Fedora) nhưng cũng có các distro được hỗ trợ bởi các công ty thương mại (RHEL, Ubuntu), dù phần mềm vẫn là nguồn mở nhưng bạn cần trả tiền cho các dịch vụ hỗ trợ.

- **Một số Distro Linux:** Red Hat Enterprise Linux (RHEL), CentOS, Fedora, Debian, Ubuntu, Gentoo.

- **Ưu điểm Linux:**
  \+ Tính bảo mật tương đối cao.
  \+ Tính linh hoạt.
  \+ Có thể hoạt động tốt trên các máy cấu hình yếu.

- **Nhược điểm:** 
\+ Số lượng ứng dụng được hỗ trợ trên Linux còn hạn chế.
\+ Một số nhà sản xuất khồn phát triển Driver hỗ trợ nền tảng Linux.

**2. Kiến trúc Linux**

![lỗi](./Image in Linux/Aspose.Words.0bd570a2-1a24-46aa-abcb-69714574a2cb.001.png)

- Kiến trúc của hệ điều hành Linux chia làm 3 phần: Kernel, Shell và Applications.
\+ **Kernel:** Đây là phần quan trọng và được ví như trái tim của HĐH, phần kernel chứa các module, thư viện để quản lý và giao tiếp với phần cứng và các ứng dụng. 

Dùng để quản lý phần cứng và các ứng dụng thực thi
* Linux xem mỗi thiết bị phần cứng tương đương với một tập tin
* Khi khởi động máy tính, Kernel được nạp vào trong bộ nhớ chính, và nó hoạt động cho đến khi tắt máy. Thực hiện chức năng mức thấp và chức năng mức hệ thống.
* Kernel chịu trách nhiệm thông dịch và gửi các chỉ thị tới bộ vi xử lý máy tính. 
* Kernel cũng chịu trách nhiệm về các tiến trình và cung cấp các đầu vào và ra cho các tiến trình

\+ **Shell:** là một chương trình có chức năng thực thi các lệnh từ người dùng hoặc từ các ứng dụng – tiện ích yêu cầu chuyển đến cho Kernel xử lý.

Chức năng của Shell:
* Thông dịch lệnh.
* Khởi tạo chương trình.
* Định hướng vào ra.
* Kết nối đường ống.
* Thao tác trên tập tin.
* Duy trì các biến.
* Điều khiển môi trường.
* Lập trình shell

\+ **Applications:** Là các ứng dụng và tiện ích mà người dùng cài đặt trên Server. Ví dụ: ftp, samba, Proxy, …

**3. File System**
- File được bố trí theo dạng cấu trúc cây, với gốc được gọi là “Root directory”. Thư mục gốc là điểm bắt đầu của hệ thống, nó sẽ tiếp tục phân làm nhiều nhánh con. Root được biểu thị bằng dấu “/”.

![lỗi](./Image in Linux/Aspose.Words.0bd570a2-1a24-46aa-abcb-69714574a2cb.002.png)

**Cấu trúc tổ chúc thư mục của Linux**

![lỗi](./Image in Linux/Aspose.Words.0bd570a2-1a24-46aa-abcb-69714574a2cb.003.png)

Nguồn: [Giải thích cấu trúc thư mục của hệ điều hành Linux - Linux Team Việt Nam (linuxteamvietnam.us)](https://linuxteamvietnam.us/giai-thich-cau-truc-thu-muc-cua-he-dieu-hanh-linux/)

**4. Command, files and directories**
Làm việc với Linux thường phải sử dụng Termial thông qua các lệnh như cài đặt 1 phần mềm, gỡ bỏ 1 phần mềm, tạo file, xoá file, copy file, ... các lệnh có thêm nhiều option khác nhau
- **Lệnh ls:** dùng để hiển thị các tập tin và thư mục có trong thư mục hiện tại.

![lỗi](./Image in Linux/Aspose.Words.0bd570a2-1a24-46aa-abcb-69714574a2cb.004.png)

- **Lệnh ls –l:** Để hiển thị các thư mục và file có trong thư mục hiện tại với nhiều thông tin hơn như: quyền read, write, excute, ngày giờ tạo, ...

![lỗi](./Image in Linux/Aspose.Words.0bd570a2-1a24-46aa-abcb-69714574a2cb.005.png)

- **Lệnh ls –a**: Hiển thị file và thư mục gồm file và thư mục ẩn trong thư mục hiện tại

![lỗi](./Image in Linux/Aspose.Words.0bd570a2-1a24-46aa-abcb-69714574a2cb.006.png)

- **Lệnh ls \*.txt:** Hiển thị các file với phần mở rộng là .txt trong thư mục hiện tại, có thể thay đổi phần mở rộng khác tùy vào mục đích.
- **Lệnh cd:** Lệnh cd dùng để di chuyển đến các thứ mục.
    \+ cd ~: di chuyển tới thư mục gốc của user hiện tại.
    \+ cd /: di chuyển tơi thư mục gốc của hệ thống.
    \+ cd ~/Desktop: di chuyển tới thư mục Desktop.
    \+ cd /usr/local/include: di chuyển đến thư mục include với đường dẫn chỉ định là usr/local/include.
    \+ cd ..: lùi về thư mục gốc.
    \+ cd ../..: lùi về thư mục gốc 2 lần

- **Lệnh pwd:** để hiển thị đường dẫn hiện tại
- **Lệnh touch:** để tạo một file trống

![lỗi](./Image in Linux/Aspose.Words.0bd570a2-1a24-46aa-abcb-69714574a2cb.007.png)

- **Lệnh cat:** dùng hiển thị nội dung của file

![lỗi](./Image in Linux/Aspose.Words.0bd570a2-1a24-46aa-abcb-69714574a2cb.008.png)

- **Lệnh mkdir:** tạo thư mục trong thư mục đang làm việc

![lỗi](./Image in Linux/Aspose.Words.0bd570a2-1a24-46aa-abcb-69714574a2cb.009.png)

- **Lệnh nano:** để mở file với trình soạn thảo nano
- **Lệnh gedit:** dùng để mở file với trình soạn thảo gedit.
- **Lệnh vi:** dùng để mở trình soạn thảo thảo vi.
- **Lệnh mv:** có 2 chức năng là đổi tên thư mục, file và di chuyển file.

\+ Đổi tên thư mục: 

![lỗi](./Image in Linux/Aspose.Words.0bd570a2-1a24-46aa-abcb-69714574a2cb.010.png)

**+** Di chuyển thư mục folderA đến folderB:

![lỗi](./Image in Linux/Aspose.Words.0bd570a2-1a24-46aa-abcb-69714574a2cb.011.png)

- **Lệnh cp:** dùng để copy thư mục và copy file

![lỗi](./Image in Linux/Aspose.Words.0bd570a2-1a24-46aa-abcb-69714574a2cb.012.png)

- **Lệnh rm:** dùng để xóa thư mục và file

![lỗi](./Image in Linux/Aspose.Words.0bd570a2-1a24-46aa-abcb-69714574a2cb.013.png)

- **Lệnh del:** dùng để xóa file

**5. Vim & nano editor:**
Là trình soan thảo Terminal
**a. GNU nano**
**Tạo file**

![lỗi](./Image in Linux/Aspose.Words.0bd570a2-1a24-46aa-abcb-69714574a2cb.014.png)

**Giao diện** 

![lỗi](./Image in Linux/Aspose.Words.0bd570a2-1a24-46aa-abcb-69714574a2cb.015.png)

- **Các tính năng của GNU nano bao gồm:** 
   \+ Hỗ trợ Autoconf
   \+ Chức năng tìm kiếm phân biệt chữ hoa chữ thường
   \+ Tìm kiếm và thay thế tương tác
   \+ Khả năng tự động thụt lề
   \+ Tùy chọn chiều rộng tab được hiển thị
   \+ Tìm kiếm và thay thế biểu thức thông thường
   \+ Công tắc chuyển đổi cho flag cmdline thông qua các phím meta
   \+ Tab completion (nhấn Tab trong khi nhập lệnh, tùy chọn hoặc tên file và môi trường shell sẽ tự động hoàn thành những gì bạn đang nhập) khi đọc/ghi file
   \+ Soft text wrapping (văn bản trông giống như đã kết thúc ở phần rìa màn hình, nhưng thực tế, nó là một dòng rất dài. Các phần tiếp theo được chỉ định bằng $)

**b. Vim**
**Tạo file**

![lỗi](./Image in Linux/Aspose.Words.0bd570a2-1a24-46aa-abcb-69714574a2cb.016.png)

**Giao diện**

![lỗi](./Image in Linux/Aspose.Words.0bd570a2-1a24-46aa-abcb-69714574a2cb.017.png)

- Vim có lợi thế là mạnh hơn GNU nano. Vim không chỉ chứa nhiều tính năng hơn, mà bạn còn có thể tùy chỉnh chương trình với các plugin và script.
- **Các tính năng của Vim bao gồm:**
   \+ Các lệnh tự động
   \+ Các lệnh bổ sung
   \+ Đầu vào
   \+ Giới hạn bộ nhớ cao hơn vanilla vi
   \+ Chia nhỏ màn hình
   \+ Khôi phục phiên
   \+ Mở rộng tab
   \+ Hệ thống tag
   \+ Thêm màu cho cú pháp
