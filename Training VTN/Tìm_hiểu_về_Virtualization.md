## Tìm hiểu về Virtualization

### I. Virtualization
**1. Virtualization là gì? (Ảo hóa)**

   Ảo hóa là tạo một phiên bản ảo của tài nguyên nào đó, chẳng hạn như server, storage device, network hoặc thậm chí operating system nơi framework chia tài nguyên thành một hoặc nhiều môi trường thực thi.

Các thiết bị, ứng dụng và người dùng có thể tương tác với tài nguyên ảo như thể nó là một tài nguyên logic thực sự duy nhất (single logical resource).  

   **Hypervisor:** phần mềm giám sát máy ảo: Là một chương trình phần mềm quản lý một hoặc nhiều máy ảo (VM). Nó được sử dụng để tạo, startup, dừng và reset lại các máy ảo. Các hypervisor cho phép mỗi VM hoặc “guest” truy cập vào lớp tài nguyên phần cứng vật lý bên dưới, chẳng hạn như CPU, RAM và lưu trữ. Nó cũng có thể giới hạn số lượng tài nguyên hệ thống mà mỗi máy ảo có thể sử dụng để đảm bảo cho nhiều máy ảo cùng sử dụng đồng thời trên một hệ thống.

   Có 2 loại hypervisor: 
**+ Loại 1: Native**

   Một hypervisor ở dạng native (hay còn gọi “bare-metal”) chạy trực tiếp trên phần cứng. Nó nằm giữa phần cứng và một hoặc nhiều hệ điều hành khách (guest operating system). Hypervisor tương tác trực tiếp với phần cứng. Là cầu nối để các VM resources sử dụng phần cứng.
   Nó phổ biến cho data center hoặc các server.

![lỗi](./Image in VM/Vm01.png)

**+ Loại 2: Hosted**

   Một hypervisor dạng hosted được cài đặt trên một máy tính chủ (host computer), mà trong đó có một hệ điều hành đã được cài đặt, chạy thông qua một hệ điều hành. Nó như là một phần mềm trên hệ điều hành.
   Dùng tốt hơn cho các máy tính cá nhân muôn sự dụng nhiều hệ điều hành.
   Ví dụ như VMware, VirtualBox,..

![lỗi](.Image in VM/Vm02.png)

**OS level:** OS-level virtualization – Ảo hóa cấp độ hệ điều hành, tạo ra nhiều máy chủ ảo chạy trên cùng một nhân hệ điều hành (cụ thể là Linux kernel). Mỗi máy chủ ảo được gọi là một container, chạy độc lập và chia sẻ với nhau toàn bộ tài nguyên của máy chủ vật lý.

![lỗi](./Image in VM/Vm03.png)

Ảo hóa cấp độ hệ điều hành, hay linux container, dựa vào 2 tính năng rất đặc biệt của Linux kernel (nhân Linux):

- **cgroups** (viết tắt của control groups): là tính năng cho phép **giới hạn, chiếm, tách biệt** việc sử dụng tài nguyên máy chủ (CPU, Memory, Disk I/O, Network…) của một tập hợp các qui trình xử lý (collection of processes)
- **namespaces**: một tính năng cho phép **phân vùng** tài nguyên của nhân Linux, đảm bảo tính độc lập trong việc sử dụng các tài nguyên của các qui trình (container) khác nhau.

Hai tính năng này giúp cho mỗi container (máy chủ ảo) độc lập với nhau, và có thể tạo, cấp phát tài nguyên, giới hạn tài nguyên tối đa cho mỗi container.

Các container có thể được cấp phát & giới hạn tài nguyên sử dụng nhưng không cố định như KVM hay Xen, … – Khi một container dùng ít tài nguyên máy chủ, thì tài nguyên này sẽ được chia sẻ cho các container khác.

Có nhiều công nghệ để triển khai mô hình máy chủ ảo theo phương thức ảo hóa hệ điều hành: OpenVZ, Virtuazzo, LXC (LXD, Solaris Containers, và Docker…

**OpenVZ**: thường được gọi là VPS giá rẻ, vì công nghệ ảo hóa của OpenVZ cho phép tạo nhiều gói VPS hơn các công nghệ Ảo hóa dựa trên phần cứng như KVM, Xen.

![lỗi](./Image in VM/Vm04.png)

**Virtuozzo**: là công nghệ dựa trên OpenVZ, nhưng được công ty Virtuozzo tích hợp thêm các tính năng thương mại, đây là một công nghệ trả phí.

Vì là phiên bản thương mại nên trước đây Virtuozzo không được sử dụng nhiều bằng người anh em miễn phí của nó – OpenVZ.

Hiện tại phiên bản mới nhất là Virtuozzo 7, cung cấp các giải pháp triển khai máy chủ ảo trên công nghệ điện toán đám mây.

Virtuozzo Cloud Infrastructure hiện nay được dùng khá nhiều bởi doanh nghiệp nhỏ, các dịch vụ cung cấp Cloud Server bình dân. Vì chi phí triển khai Cloud IaaS với Virtuozzo khá rẻ.

**LXC:** viết tắt của Linux Container, là phương thức Ảo hóa cấp độ hệ điều hành, cho phép chạy nhiều máy ảo dưới dạng Container trên HĐH Linux.

LXC được phát triển sau OpenVZ, là dự án đóng góp bởi nhiều cá nhân và tập đoàn lớn như IBM, Google và cả Virtuozzo nữa.

Hiện nay, một phiên bản cải tiến của LXC là LXD – dự án mã nguồn mở được phát triển bởi Canonical – công ty đứng sau HĐH Ubuntu. LXD nâng cấp các tính năng quản lý container, nâng cấp bảo mật, HA (High Availability), …

![lỗi](./Image in VM/Vm05.png)

**Docker:** Docker là một nền tảng điện toán đám mây dùng phương thức Ảo hóa cấp độ hệ điều hành để cung cấp phần mềm được đóng gói dưới dạng Linux container.

Lúc đầu, Docker sử dụng LXC như môi trường thực thi, trình điều khiển container mặc định. Từ phiên bản 1.1 trở đi, Docker thay thế LXC bằng trình điều khiển riêng (own component).

Khác với LXC, Virtuozzo, LXC, OpenVZ… Docker không phải là công nghệ dành cho triển khai Cloud Server (IaaS) mà nó là một sản phẩm thuộc PaaS – cung cấp platform cho phép triển khai các software trên đám mây dễ dàng hơn.

![lỗi](./Image in VM/Vm06.png)

Ưu điểm vượt trội của Docker là tính đóng gói (package) và tính đồng nhất.  Điều này giúp cho nó trở thành công cụ phát triển chóng mặt trong Điện toán đám mây.

Với Docker, bạn có thể đóng gói mọi thứ và mang nó đi triển khai trên bất kỳ Cloud Server nào, giống như kiểu nhân bản vô tính hàng loạt vậy.


**2. Virtual Machine (VM)**

   **Virtual Machine (VM)** là một chương trình giả lập một hệ thống máy tính, được chạy trên hệ điều hành chủ và hoạt động như một máy tính thật.

Một máy ảo cung cấp phần cứng ảo (bao gồm CPU, RAM, ổ đĩa cứng) để chạy hệ điều hành và các phần mềm riêng trên đó. Các phần cứng ảo này được ánh xạ tới phần cứng thực trên máy tính vật lý, ví dụ như là ổ đĩa cứng ảo được lưu trong một file đặt trên ổ đĩa cứng thực. Máy ảo chạy sẽ chia sẻ tài nguyên phần cứng với máy thực, do đó khi thiết lập máy ảo cần tinh chỉnh phần cứng ảo không nên vượt quá khả năng xử lý của máy tính thực.

**Các công nghệ tạo VM**

- **Xen**:  Xen là một phần mềm Hypervisor Type – 1, ra đời từ Phòng thí nghiệm máy tính của Đại học Cambridge (Anh) và hiện nay được tiếp tục phát triển bởi Linux Foundation và được hỗ trợ chính bởi tập đoàn Intel.

![lỗi](./Image in VM/VM07.png)

Công nghệ của Xen hỗ trợ Ảo hóa dựa trên phần cứng, và được sử dụng rất rộng rãi trên thị trường IaaS, có thể kể tới Amazon EC2, IBM SoftLayer, Rackspace Cloud, Liquid Web hay Fujitsu Global Cloud Platform, OrionVM..

Xen là một dự án Open source, miễn phí. Nhưng nó cũng được phát triển thành các phiên bản thương mại như dự án Citrix XenServers, Huawei FusionSphere, Oracle VM Server for x86, ..

Trên nền tảng Cloud Computing, Xen đóng vai trò Hypervisor Type-1 cho các phần mềm triển khai Cloud Server IaaS như CloudStack, OpenStack, Hyper-V, Open Nebula…

- **KVM:**. Kernel-based Virtual Machine hay Máy ảo dựa trên Nhân (Kernel) là tên gọi của một module cho phép biến Linux Kernel (nhân Linux) hoạt động như một Hypervisor.

KVM được tạo ra bởi Qumranet, Inc vào năm 2006, sau Xen 3 năm. Hiện tại KVM được tiếp tục phát triển bởi Open Virtualization Alliance (OVA), đây là một dự án riêng cũng nằm dưới sự quản lý của Tổ chức Linux Foundation.

Về tính năng, KVM không khác Xen quá nhiều, ngoài một số cải tiến nhỏ.

Hiện nay KVM được dùng rất phổ biến, tiêu biểu như Google Compute Engine, Vultr, DigitalOcean, OVH…

Tương tự Xen, các phần mềm triển khai Cloud Computing IaaS như OpenStack, CloudStack, OpenNebula, … dùng KVM như Hypervisor Type-1.

![lỗi](./Image in VM/Vm08.png)

- **Vmware ESXi:** VMware ESXi là phần mềm Hypervisor Type-1 của VMware – tập đoàn ảo hóa số 1 thế giới. Trước đay ESXi có tên là VMware ESX – viết tắt của VMware Elastic Sky X.

VMware ESXi cũng có tính năng như Xen, KVM, nhưng là sản phẩm thương mại.

Hiện nay, ESXi là một phần của bộ công cụ triển khai Cloud Computing IaaS của VMware là vSphere  (VMware Infrastructure).

![lỗi](./Image in VM/Vm09.png)

- **Hyper-V**:  là một Hypervisor Type-1 độc quyền dành cho hệ điều hành Windows Server. Hiện nay Hyper-V cũng cung cấp giải pháp Paravirtualization (Ảo hóa song song) để hỗ trợ các HĐH nhân Linux. Không như Xen, KVM, VMware ESXi, Hyper-V phổ biến nhờ ‘kí sinh’ Windows Server.

![lỗi](./Image in VM/Vm010.png)

- **VirtualBox:** là một phần mềm hỗ trợ tạo máy ảo đa nền tảng, có thể thực thi trên các máy tính sử dụng chip Intel hoặc AMD. VirtualBox hỗ trợ chạy nhiều HĐH ảo cùng lúc. VirtualBox chạy được trên nhiều HĐH khác nhau, cả 32-bit lẫn 64-bit: Windows, Mac, Linux, Solaris.

### II. Virtual machine manager (VMM)

**VMM** là một trong nhiều kỹ thuật ảo hóa phần cứng cho phép nhiều hệ điều hành, gọi là Guests, chạy đồng thời trên một máy tính chủ. VMM cho các hệ điều hành khách một nền tảng điều hành ảo và quản lý việc thực thi các hệ điều hành khách. Nhiều phiên bản của nhiều hệ điều hành khác nhau có thể chia sẻ tài nguyên phần cứng được ảo hóa.

Virtual Machine Manager cho phép người dùng:

\+ Tạo, chỉnh sửa, khởi động và dừng máy ảo

\+ Xem và điều khiển từng bảng điều khiển của máy ảo

\+ Xe thống kê hiệu suất và mức sử dụng cho mỗi máy ảo

\+ xem tất cả các máy ảo và máy chủ đang chạy cũng như số liệu thống kê về hiệu suất trực tiếp hoặc sử dụng tài nguyên của họ.

\+ Sử dụng máy ảo KVM, Xen hoặc QEMU, chạy cục bộ hoặc từ xa.

**Các loại mạng của libvirt**

**Bridged network:** sẽ chia sẻ một thiết bị Ethernet thật với các máy ảo VM. Mỗi VM có thể gán trực tiếp bất kì địa chỉ IP trên mạng LAN, như một máy tính vật lý. Bridging cho phép hiệu năng cao nhất. Trong chế độ này libvirt server sẽ được kết nối trực tiếp tới LAN thông qua Ethernet và sẽ sử dụng luôn card mạng vật lý.

**NAT mode:** một  a virtual network switch hoạt động ở chế độ NAT mode (sử dụng IP masquerading hơn là SNAT hoặc DNAT).
  Bất kì máy ảo nào được kết nối tới nó, sử sử dụng địa chỉ IP của máy host để liên lạc ra bên ngoài. Các máy ở mạng ngoài không thể liên lạc với máy guest ở bên trong khi virtual network switch hoạt động trong chế độ NAT.

![lỗi](./Image in VM/Vm011.png)

__*Hạn chế*__: Ở chế độ NAT có sử dụng iptables rules. Do đó có thể sẽ bị ngăn bởi một vài rule khi sử dụng, nên cần phải cẩn thận.

**Routed mode:** Với chế độ định tuyến, công tắc ảo được kết nối với mạng LAN vật lý, truyền lưu lượng truy cập mạng của khách qua lại mà không cần sử dụng NAT.

Virtual Switch thấy địa chỉ IP trong mỗi gói, sử dụng thông tin đó khi quyết định phải làm gì.

Trong chế độ này, tất cả các máy ảo đều nằm trong một mạng con được định tuyến thông qua Virtual Switch. Tuy không máy ảo nào trong mạng vật lý biết sự tồn tại của mạng con này hoặc làm sao để tìm được ra nó. Do đó, cần phải định cấu hình các bộ định tuyến trong mạng vật lý (ví dụ: sử dụng tuyến tĩnh). 

![lỗi](./Image in VM/Vm012.png)

**Isolated mode:** Trong chế độ này, các máy Guest được kết nối với switch ảo có thể giao tiếp với nhau và với máy chủ. Tuy nhiên, lưu lượng truy cập của họ sẽ không vượt qua bên ngoài máy chủ, cũng như họ không thể nhận được lưu lượng truy cập từ bên ngoài máy chủ.

Việc sử dụng dnsmasq trong chế độ này là có thể và trên thực tế cần thiết vì nó được sử dụng để trả lời các DHCP request. 

Tuy nhiên, ngay cả khi mạng này bị cô lập với bất kỳ mạng vật lý nào, tên DNS vẫn được giải quyết. Do đó, người ta có thể rơi vào tình huống DNS được giải quyết nhưng khách không thể ping.

![lỗi](./Image in VM/Vm013.png)

### III. Nguồn tham khảo:

[\[ KVM \] Tổng quan về Virtualization và Hypervisor - Trang tin tức từ Cloud365 - Nhân Hòa](https://news.cloud365.vn/kvm-tong-quan-ve-virtualization-va-hypervisor/)

[What is Virtualization? | IBM](https://www.ibm.com/cloud/learn/virtualization-a-complete-guide)

[Các công nghệ ảo hóa (miichisoft.net)](https://tech.miichisoft.net/cac-cong-nghe-ao-hoa/)

[Virtual Machine Manager – Wikipedia tiếng Việt](https://en.wikipedia.org/wiki/Virtual_Machine_Manager)

[What is Virtual Machine Manager (VMM)? | DDI (Secure DNS, DHCP, IPAM) | Infoblox](https://www.infoblox.com/glossary/virtual-machine-manager-vmm/)

[VirtualNetworking - Libvirt Wiki](https://wiki.libvirt.org/page/VirtualNetworking#Routed_mode)

[So sánh các công cụ Container : OpenShift, Kubernetes và Docker - Smart Industry VN](https://smartindustry.vn/technology/edge-computing/so-sanh-nhanh-openshift-kubernetes-va-docker/)

[Tìm hiểu về Containers | CloudFun](https://cloudfun.vn/threads/tim-hieu-ve-containers.318/)

[Virtualization vs Containerization | Baeldung on Computer Science](https://www.baeldung.com/cs/virtualization-vs-containerization)



