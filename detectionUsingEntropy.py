#Appendix C: Collection and Entropy computation

import math
import time
from pox.core import core

log = core.getLogger()

class Entropy(object):
      count = 0
      destFrequency = {}
      destIP = []
      destEntropy = []
      value = 1
      start_list = []

      def collectStats(self, element):
          l = 0
          self.count += 1
          self.destIP.append(element)
          if self.count == 50:
             for i in self.destIP:
                 l += 1
                 if i not in self.destFrequency:
                    self.destFrequency[i] = 0
                 self.destFrequency[i] += 1
                 if self.destFrequency[i] > 2:	#######
                    self.resp_timer()	#######
             self.findEntropy(self.destFrequency)
             log.info(self.destFrequency)

             self.destFrequency = {}
             self.destIP = []
             l = 0
             self.count = 0

      def resp_timer(self):	#######
      	   start = time.time()	#######
      	   self.start_list.append(start)	#######
      	   return self.start_list[0]	#######
      
      def findEntropy (self, lists):
	  l = 50
	  entropyList = []  
	  for k,p in lists.items():
	      c = p/float(l)
	      c = abs(c)
	      entropyList.append(-c * math.log(c, 10))

	      log.info('Entropy = ') 
	      log.info(sum(entropyList))

	      self.destEntropy.append(sum(entropyList))

	  if(len(self.destEntropy)) == 50:
	      print self.destEntropy
	      self.destEntropy = []
	  self.value = sum(entropyList)
	
      def __init__(self):
	  pass

