﻿## Ansible Overview

**1. Ansible là gì?**
- **Ansible** là một trong những công cụ quản lý cấu hình hiện đại, nó tạo điều kiện thuận lợi cho công việc cài đặt, quản lý và bảo trì các server từ xa, với thiết kế tối giản giúp người dùng cài đặt và chạy nhanh chóng.
- Người dùng viết các tập lệnh cấp phép Ansible trong YAML, một tiêu chuẩn tuần tự hóa dữ liệu thân thiện với người dùng, chúng không bị ràng buộc với bất kỳ ngôn ngữ lập trình nào. Chính vì vậy người dùng có thể tạo ra các tập lệnh cấp phép phức tạp một cách trực quan hơn so với các công cụ còn lại trong cùng danh mục.
- Ansible không yêu cầu người dùng phải cài đặt thêm bất kỳ phần mềm đặc biệt nào. Một máy điều khiển được cài đặt tích hợp trong phần mềm Ansible, và giao tiếp với các nút thông qua SSH tiêu chuẩn.
- Là công cụ quản lý cấu hình và tự động hóa, Ansible gói gọn tất cả các tính năng phổ biến có trong các công cụ khác cùng loại, trong khi vẫn đáp ứng được tính đơn giản và hiệu suất.

![lỗi](./ImageAnsible/As1.png)

**Lý do nên sử dụng Ansible để quản lý cầu hình:**

\+ Là một opensource và được sử dụng miễn phí.

\+ Sử dụng phương thức ssh

\+ Không tốn nhiều tài nguyên khi cài đặt

\+ Được phát triển bởi ngôn ngữ python, khi tạo thêm module cũng sẽ sử dụng ngôn ngữ này.

\+ Khá nhẹ và dễ setup

\+ Các sciprt chủ yếu được dùng định dạng YAML

\+ Trên Ansible có một cộng đồng tương tác lớn, dễ dàng giao lưu học hỏi.

**Một số thuật ngữ cơ bản khi sử dụng Ansible**

- **Controller Machine:** Là máy cài Ansible, nó sẽ chịu trách nhiệm quản lý, điều khiển và gửi các task đến những máy con cần quản lý.
- **Inventory:** Là file chứa thông tin những server cần quản lý. File này thường nằm tại đường dẫn /etc/ansible/hosts.
- **Playbook:** Là file chứa các task được ghi dưới định dạng YAML. Máy controller sẽ đọc các task này trong Playbook sau đó đẩy các lệnh thực thi tương ứng bằng Python xuống các máy con.
- **Task:** Một block ghi lại những tác vụ cần thực hiện trong playbook và các thông số liên quan.
- **Module:** Trong Ansible có rất nhiều module khác nhau. Ansible hiện có hơn 2000 module để thực hiện các tác vụ khác nhau, bạn cũng có thể tự viết thêm những module của mình khi có nhu cầu. Một số Module thường dùng cho những thao tác đơn giản như: System, Commands, Files, Database, Cloud, Windows,…
- **Role:** Là một tập playbook đã được định nghĩa để thực thi 1 tác vụ nhất định. Nếu bạn có nhiều server, mỗi server thực hiện những tasks riêng biệt. Và khi này nếu chúng ta viết tất cả vào cùng một file playbook thì khá là khó để quản lý. Do vậy roles sẽ giúp bạn phân chia khu vực với nhiệm vụ riêng biệt.
- **Play:** là quá trình thực thi một playbook.
- **Facts:** Thông tin của những máy được Ansible điều khiển, cụ thể sẽ là các thông tin về OS, system, network,…
- **Handlers:** Được sử dụng để kích hoạt những thay đổi của dịch vụ như start, stop service.
- **Variables:** Được dùng để lưu trữ các giá trị và có thể thay đổi được giá trị đó. Để khai báo biến, người dùng chỉ cần sử dụng thuộc tính vars đã được Ansible cung cấp sẵn.
- **Conditions:** Ansible cho phép người dùng điều hướng lệnh chạy hay giới hạn phạm vi để thực hiện câu lệnh nào đó. Hay nói cách khác, khi thỏa mãn điều kiện thì câu lệnh mới được thực thi. Ngoài ra, Ansible còn cung cấp thuộc tính Register, một thuộc tính giúp nhận câu trả lời từ một câu lệnh. Sau đó ta có thể sử dụng chính kết quả đó để chạy những câu lệnh sau.

**2. Kiến trúc của Ansible**

![lỗi](./ImageAnsible/As2.png)

- Ansible sử dụng kiến trúc agentless không cần đến agent để giao tiếp với các máy khác. Cơ bản nhất là giao tiếp thông qua các giao thức WinRM trên Windows, SSH trên Linux hoặc giao tiếp qua chính API của thiết bị đó cung cấp.
- Ansible có thể giao tiếp với rất nhiều OS, platform và loại thiết bị khác nhau từ Ubuntu, VMware, CentOS, Windows cho tới Azure, AWS, các thiết bị mạng Cisco và Juniper,… mà hoàn toàn không cần agent khi giao tiếp.
- Nhờ vào cách thiết kế này đã giúp làm tăng tính tiện dụng của Ansible do không cần phải cài đặt và bảo trì agent trên nhiều host. Có thể nói rằng đây chính là một thế mạnh của Ansible so với các công cụ có cùng chức năng như Chef, SaltStack, Puppet (trong đó Salt có hỗ trợ cả 2 mode là agent và agentless).

**Ứng dụng của Ansible**

- **Provisioning:** Khởi tạo VM, container hàng loạt trên cloud dựa trên API – OpenStack, AWS, Google Cloud, Azure…
- **Configuration Management:** Quản lý cấu hình tập trung các dịch vụ, không cần phải tốn công chỉnh sửa cấu hình trên từng server.
- **Application Deployment:** Deploy ứng dụng hàng loạt, giúp quản lý hiệu quả vòng đời của ứng dụng từ giai đoạn dev cho đến production.
- **Security & Compliance:** Quản lý các chính sách về an toàn thông tin một cách đồng bộ trên nhiều sản phẩm và môi trường khác nhau như deploy policy hay cấu hình firewall hàng loạt trên nhiều server,…

![lỗi](./ImageAnsible/As3.png)

**3. Cài đặt Ansible**

**# Cài đặt trên Ubuntu**
```
$ sudo apt update
$ sudo apt install software-properties-common
$ sudo apt-add-repository ppa:ansible/ansible
$ sudo apt update
$ sudo apt install ansible
```

![lỗi](./ImageAnsible/As4.png)

![lỗi](./ImageAnsible/As5.png)

### Cấu hình

**Tạo ssh key** 
```
$ ssh-keygen –t ed22519 –C “My SSH key”
```
Trong đó ssh-keygen là phương thức tạo file mã hóa Pass và ed22519 là chuẩn mã hóa, sau khi enter nó sẽ tạo ra 2 file trong máy

![lỗi](./ImageAnsible/As6.png)

Để có thể truy cập đến máy thực thi ra phải sao chép file Pass Mã hóa đến máy thực thi bằng câu lệnh dưới, file cần sao chép là id_ed25519.pub
```
$ ssh-copy-id –i $HOME/.ssh/id_ed22519.pub ubuntu@192.168.194.132
```
User là ubuntu và 192.168.194.132 là Ip của máy thực thi, ở máy thực thi sau khi copy key sẽ có dòng key mã hóa này

![lỗi](./ImageAnsible/As7.png)

Test thử bằng cách ssh vào máy thực thi
```
$ ssh ubuntu@192.168.194.132
```
Sẽ thấy không còn yêu cầu pass khi truy cập ssh nữa. Tiếp đó sẽ tạo một file inventory với lệnh:
```
$ sudo vi inventory
```
Trong khung edit write điền vào địa chỉ IP của máy thực thi lệnh giống trong hình cho file inventory

![lỗi](./ImageAnsible/As8.png)

Thực hiện test thử khi chạy 2 lệnh “runtime” bà “lsb_release –a” để thấy được thời gian và hệ điều hành của máy

![lỗi](./ImageAnsible/As9.png)

Tạo fie date.yml

![lỗi](./ImageAnsible/As10.png)

Bắt đầu build, sẽ có kết quả như sau:

![lỗi](./ImageAnsible/As11.png)

Có thể thêm vài thông tin máy chủ vào file inventory để thực thi file một cách bảo mật hơn cho việc quản lý các máy thực thi

![lỗi](./ImageAnsible/As12.png)

Thực hiện lệnh mã hóa để tạo file passwords.yml để tăng bảo mật

![lỗi](./ImageAnsible/As13.png)

File password khi tạo 

![lỗi](./ImageAnsible/As14.png)

Toàn bộ đã được mã hóa, bắt đầu chạy dòng lệnh dưới để thực hiện Playbook date.yml, nó sẽ yêu cầu nhập password và bảo vệ file passwords.yml

![lỗi](./ImageAnsible/As15.png)

**4. Cách thức Debug với Ansible**
- Cách cơ bản nhất là chạy ansible/ansible-playbook với mức độ chi tiết tăng bằng cách thêm -vvv vào dòng thực thi.
- Để bật gỡ lỗi và tăng độ verbosity trong Ansible, có thể vượt qua các biến môi trường tương ứng trên dòng lệnh hoặc xác định các thiết đặt này trong tệp cấu hình Ansible.
- Bật gỡ lỗi và tăng tính verbosity trong Ansible

Chạy playbook với enable debug và với level verbosity maximum:
```
$ ANSIBLE_DEBUG=true ANSIBLE_VERBOSITY=4 ansible-playbook playbook.yml
```
Hoặc
```
$ ANSIBLE\_DEBUG=true -vvvv ansible-playbook playbook.yml
```
Cũng có thể xác định các cài đặt này để bật debug trong Ansible và thiết lập level verbosity:  ansible.cfg

|Option|Description|
| --- | --- |
|default_verbosity=[0\|1\|2\|3\|4]|Set the default verbosity level|
|default_debug=[false\|true]|Enable the debug output|

**5. Plugins và ứng dụng của các plugins**

**Plugin** là những phần mã làm tăng chức năng cốt lõi của Ansible. Ansible sử dụng kiến trúc plugin để kích hoạt một bộ tính năng phong phú, linh hoạt và có thể mở rộng.

**Các loại plugins**

**+ Action Plugins:** hoạt động cùng với các mô-đun để thực hiện các hành động theo yêu cầu của các tác vụ playbook. Chúng thường thực thi tự động trong nền thực hiện công việc tiên quyết trước khi các mô-đun thực thi. 

**+** **Become Plugins:** Become Plugins hoạt động để đảm bảo rằng Ansible có thể sử dụng các hệ thống leo thang đặc quyền nhất định khi chạy các lệnh cơ bản để làm việc target machine cũng như các mô-đun cần thiết để thực hiện các tác vụ được chỉ định trong Play.

**+ Cache Plugins:** cho phép Ansible lưu trữ các dữ liệu đã thu thập hoặc dữ liệu từ inventory mà không cần truy xuất chúng từ nguồn.

Cache Plugin mặc định là memory plugin, chỉ lưu trữ dữ liệu cho quá trình thực thi của Ansible. Các plugin khác có khả năng lưu trữ liên tục có sẵn để cho phép lưu dữ liệu vào bộ nhớ đệm trong các lần chạy. Một số Cache plugin ghi vào tệp, những plugin khác ghi vào cơ sở dữ liệu.

**+ Callback Plugins:** cho phép thêm các behaviors mới vào Ansible khi respond events. Theo mặc định, Callback plugin kiểm soát hầu hết đầu ra khi chạy các chương trình dòng lệnh, nhưng cũng có thể được sử dụng để thêm đầu ra bổ sung, tích hợp với các công cụ khác và sắp xếp các events  thành phần lưu trữ phụ.

**+ Cliconf Plugins:** cung cấp một giao diện tiêu chuẩn để Ansible thực thi các tác vụ trên các thiết bị mạng đó. Ansible tải Cliconf plugin thích hợp tự động dựa trên variable ansible_network_os.

**+ Connection Plugins:** cho phép Ansible kết nối với các target hosts để nó có thể thực thi các tác vụ trên hosts. Ansible đi kèm với nhiều Connection plugin, nhưng mỗi máy chủ chỉ có thể sử dụng một plugin tại một thời điểm.

**+ Httpapi Plugins:** cho Ansible biết cách tương tác với API dựa trên HTTP của một thiết bị từ xa và thực thi các tác vụ trên thiết bị.

Mỗi plugin đại diện cho một dialect cụ thể của API. Một số dành riêng cho nền tảng (Arista eAPI, Cisco NXAPI), trong khi một số khác có thể sử dụng được trên nhiều nền tảng khác nhau (RESTCONF). Ansible tự động tải plugin httpapi thích hợp dựa trên variable ansible_network_os.

**+ Inventory Plugins:** cho phép người dùng trỏ vào các nguồn dữ liệu để biên dịch inventory của hosts mà Ansible sử dụng để target tasks, bằng cách sử dụng tham số dòng lệnh -i / path / to / file và / hoặc -i 'host1, host2' hoặc từ configuration sources.

**+ Lookup Plugins:** là một phần mở rộng dành riêng cho Ansible dành cho ngôn ngữ Jinja2 templating. Có thể sử dụng Lookup plugin để truy cập dữ liệu từ các nguồn bên ngoài (tệp, cơ sở dữ liệu, kho khóa / giá trị, API và các dịch vụ khác) trong playbooks.. Ansible làm cho dữ liệu được trả về bởi một Lookup plugin có sẵn bằng cách sử dụng hệ thống tạo templates tiêu chuẩn. ngoài.

**+ Netconf Plugins:** cung cấp một giao diện tiêu chuẩn cho Ansible để thực hiện các tác vụ trên các thiết bị mạng đó. Ansible sẽ load plugin netconf thích hợp tự động dựa trên variable. Nếu nền tảng hỗ trợ triển khai standard Netconf, Ansible sẽ tải plugin netconf. Nếu nền tảng hỗ trợ netconf RPCs, Ansible sẽ load plugin netconf dành riêng cho nền tảng.

**+ Shell Plugins:** Các shell plugin hoạt động để đảm bảo rằng các lệnh cơ bản Ansible chạy được định dạng đúng để làm việc với target hosts và cho phép người dùng cấu hình một số behaviors  nhất định liên quan đến cách Ansible thực hiện các tác vụ.

**+ Strategy Plugins:**  kiểm soát luồng thực hiện phát bằng cách xử lý tác vụ và lập lịch hosts.

**+ Vars Plugins:** đưa dữ liệu biến bổ sung vào các lần chạy Ansible không đến từ inventory source, playbook hoặc dòng lệnh. Các cấu trúc Playbook như ‘host_vars’ và ‘group_vars’ hoạt động bằng cách sử dụng cácVars plugin.

**+ Using filters to manipulate data:** chuyển đổi dữ liệu JSON thành dữ liệu YAML, chia URL để trích xuất tên máy chủ, lấy SHA1 hash của chuỗi, thêm hoặc nhân số nguyên và nhiều hơn nữa. 

**+ Tests:** các bài test trong Jinja là một cách đánh giá các biểu thức mẫu và trả về True hoặc False. Test cũng có thể được sử dụng trong các list processing filters  như map () và select () để chọn các mục trong danh sách.

**+ Rejecting modules:** Nếu muốn tránh sử dụng một số mô-đun nhất định, có thể thêm chúng vào reject list để ngăn Ansible loading nó. Để Rejecting plugin, hãy tạo tệp cấu hình yaml. Có thể chọn một đường dẫn khác cho reject list bằng cách cài đặt PLUGIN_FILTERS_CFG trong phần ansible.cfg.

**6. Tài liệu tham khảo**

[Ansible là gì? Những khái niệm cơ bản khi bắt đầu tìm hiểu về Ansible](https://itnavi.com.vn/blog/ansible-la-gi/)

[Hướng dẫn và sử dụng Ansible](https://vngeeks.com/huong-dan-va-dung-ansible/)

[Cài đặt và cấu hình](https://bizflycloud.vn/tin-tuc/ansible-phan-1-cai-dat-va-cau-hinh-412.htm)

[Ansible Làm quen với tự động hóa bằng Ansible](https://www.itblognote.com/2019/04/ansible-phan-1-lam-quen-voi-tu-ong-hoa.html)

[debugging — Làm thế nào để gỡ lỗi các vấn đề Ansible?](https://www.it-swarm-vi.com/vi/debugging/lam-nao-de-go-loi-cac-van-de-ansible/829681335/)

[Ansible: Enable Debug and Increase Verbosity](https://www.shellhacks.com/ansible-enable-debug-increase-verbosity/#:~:text=To%20enable%20debug%20and%20increase%20verbosity%20in%20Ansible,debug%20mode%20should%20not%20be%20used%20in%20production.)

[Working With Plugins](https://docs.ansible.com/ansible/latest/plugins/plugins.html)


