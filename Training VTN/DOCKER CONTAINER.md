﻿## DOCKER CONTAINER

**1. Tổng quan**

**Docker** là một nền tảng mã nguồn mở để xây dựng, triển khai và quản lý các ứng dụng được containers (trên nền tảng ảo hóa). Docker cung cấp cách để building, deloying và running ứng dụng dễ dàng bằng cách sử dụng containers.

**Docker** cho phép các nhà phát triển đóng gói ứng dụng vào container. Các thành phần thực thi được chuẩn hóa kết hợp mã nguồn ứng dụng với tất cả các thư viện. Và nó phụ thuộc của hệ điều hành (OS) cần thiết để chạy mã trong bất kỳ môi trường nào.

**Docker trong Container:**
Docker cho phép các lập trình viên đóng gói các ứng dụng cần thiết vào trong containers, như thư viện, gói dưới dạng package. Nhờ vào container, ứng dụng sẽ chạy trên mọi máy Linux bất kể mọi tùy chỉnh và cài đặt khác với máy dùng để viết code.
Docker khá giống với Virtual Machine, nhưng Docker phát triển mạnh mẽ và phổ biến nhanh chóng hơn. Dưới đây là một số nguyên nhân:
- Dễ sử dụng: Docker rất dễ sử dụng có các developer, Admin System,… vì nó tận dụng container để build và kiểm tra nhanh chóng. Và nó có thể đóng gói các ứng dụng trên laptop của họ và chạy trên public cloud, private cloud,…
- Tốc độ: Nói về tốc độ thì docker containers rất nhẹ và nhanh và có thể khởi tạo và chạy docker container chỉ trong vài giây.
- Môi trường chạy: Có thể tận dụng và chia nhỏ các container riêng lẻ. Ví dụ bạn có thể chạy Database trên một container và Redis cache chạy trên một container khác trong khi ứng dụng Node.js lại có thể chạy trên một container khác nữa. Khi sử dụng Docker rất dễ để liên kết các container với nhau để tạo thành một ứng dụng. Từ đó, làm cho nó dễ dang scale và update các thành phần độc lập với nhau.

**Docker** sử dụng kiến trúc client-server. Docker client sẽ liên lạc với các Docker daemon, các Docker daemon sẽ thực hiện các tác vụ build, run và distribuing các Docker container. Cả Docker client và Docker daemon có thể chạy trên cùng 1 máy, hoặc có thể kết nối theo kiểu Docker client điều khiển các docker daemon như hình trên. Docker client và daemon giao tiếp với nhau thông qua socket hoặc RESTful API.

**Docker daemon** chạy trên các máy host. Người dùng sẽ không tương tác trực tiếp với các daemon, mà thông qua Docker Client.

**Kiến trúc của Docker**

![lỗi](./ImageDocker/Dock1.png)

**Các khái niệm khác**

**DockerFile**
Mọi container Docker bắt đầu bằng một file văn bản đơn giản chứa hướng dẫn về cách tạo image container Docker. DockerFile tự động hóa tiến trình tạo image Docker. Về cơ bản, đây là danh sách các lệnh mà Docker Engine sẽ chạy để tập hợp image.

**Docker Images**
Chứa mã nguồn ứng dụng thực thi cũng như tất cả các công cụ, thư viện. Kèm theo dependencies mà ứng dụng cần để chạy dưới dạng container. Khi chạy Docker image, nó sẽ trở thành một phiên bản (hoặc nhiều phiên bản) của container.

Mỗi khi một container được tạo từ Docker image, một lớp mới khác được gọi là lớp container được tạo. Các thay đổi được thực hiện đối với container. Chẳng hạn như nó có thể thêm hoặc xóa file chỉ được lưu vào lớp container và chỉ tồn tại khi container đang chạy. Quá trình tạo image lặp đi lặp lại này giúp tăng hiệu quả tổng thể. Bởi nhiều phiên bản container có thể chạy chỉ từ một base image duy nhất. Do đó, khi chúng làm như vậy, chúng sẽ tận dụng một ngăn xếp chung.

![lỗi](./ImageDocker/Dock2.png)


**Docker containers**

Docker container là các phiên bản live, running instance của Docker image. Ta thấy Docker image là file chỉ đọc còn container là phiên bản live, executable và người dùng có thể tương tác với chúng. Cùng với đó, quản trị viên có thể điều chỉnh cài đặt và các quy định của họ.

**Docker Hub**
Là kho lưu trữ công khai Docker image. Nó tự gọi mình là “thư viện và cộng đồng lớn nhất thế giới về image container”. Nó chứa hơn 100.000 image container và chúng được lấy từ các nhà cung cấp phần mềm thương mại, các dự án mã nguồn mở, các nhà phát triển cá nhân. Nó bao gồm các image được sản xuất bởi Docker, Inc. Cùng với đó là các image được chứng nhận thuộc Cơ quan đăng ký tin cậy Docker và hàng nghìn image khác.

**Docker Client**
Đây là thành phần mà bạn có thể tương tác với Docker thông qua command line. Docker client sẽ gửi lệnh tới Docker Deamon thông qua REST API.


**2. Cấu hình Proxy**

**Cấu hình Docker sử dụng HTTP Proxy Sever**

Đây là mô hình đầu tiên đó là cấu hình thông tin HTTP Proxy Server để dịch vụ Docker có thể kết nối ra Internet Public. Docker client sẽ tạo các truy vấn REST đến dịch vụ Docker, từ đó dịch vụ Docker sẽ đảm nhận các phần việc còn lại.

![lỗi](./ImageDocker/Dock7.png)

- B1:  Tạo một file chứa thông tin biến môi trường về Proxy Server dành cho Docker sử dụng
 ```
# vi /etc/sysconfig/docker
http_proxy="http://10.13.92.251:3128"
https_proxy="http://10.13.92.251:3128"
no_proxy="127.0.0.1, localhost"

 ```
- B2: Xác định file khởi động của dịch vụ Docker.
```
# systemctl status docker
● docker.service - Docker Application Container Engine
Loaded: loaded (/usr/lib/systemd/system/docker.service; enabled; vendor preset: disabled)
```
- B3:  Thêm dòng cấu hình sau vào file này dưới section [Service] , để khi dịch vụ Docker khởi động lại sẽ load các biến môi trường từ file ‘/etc/sysconfig/docker‘ mà bạn quy định.
```
# vi /usr/lib/systemd/system/docker.service

[Service]
EnvironmentFile=/etc/sysconfig/docker
```
- B4: Khởi động lại dịch vụ Docker để load thông tin biến môi trường.
```
# systemctl daemon-reload
# systemctl restart docker
```

**Private Docker Registry**
- Private Docker Registry cho phép chúng ta chứa images dùng riêng tư trong phạm vi của công ty, ngoài ra cải thiện băng thông khi tốc độ truy cập đến các public docker registry có vấn đề.
![lỗi](./ImageDocker/Dock8.png)

    - **Cài đặt Docker Registry cho localhost**
    Trên Server mà cài đặt docker và docker-compose, chúng ta thực hiện triển khai cài đặt docker register như sau:
        - Tạo thư mục để lưu các images
        ```
        sudo mkdir -p /srv/registry
        ```
        - Start registry container
        ```
        docker run -d -p 5000:5000 -v /srv/registry:/var/lib/registry –restart=always –name registry registry:2
        ```
        Lệnh trên sẽ thực hiện: expose cổng từ container ra docker host với port 5000
    - **Cài đặt Docker Registry dùng local network**
    Nếu chỉ cài đặt docker registry cho localhost, thì phạm vi sử dụng image chỉ dành riêng cho host đó. Để cho phép sử dụng Docker Registry trong phạm vi toàn mạng, chúng ta cần thiết lập thêm một số tùy chọn.
        - **B1: Thiết lập certificate cho docker registry**
        Đăng ký cert SSL cho tên miền: example.local
        Tạo tệp tin /etc/docker/daemon.json với nội dung sau
        ```
        {   
        "insecure-registries" : ["hub.example.local:5000"] 
        }
        ```
        Tạo self-signed certificates
        ```
        sudo mkdir -p /etc/certs
        sudo openssl req \
        -newkey rsa:4096 -nodes -sha256 -keyout /etc/certs/hub.example.local.key \
        -x509 -days 365 -out /etc/certs/hub.example.local.crt
        ```
        Khi đó chúng ta có key hub.example.local.key và cert hub.example.local.crt
        **Stop registry nếu đang chạy**
        ```
        docker container stop registry
        ```
        - **B2: Start registry container**
        Chúng ta thiết lập biến môi trường, để chỉ định đường dẫn chứa các tệp tin hub.example.local.crt và hub.example.local.key . Registry chạy trên https mặc định với port 443
        ```
        docker run -d --restart=always --name registry \
        -v /etc/certs:/certs -v  /srv/registry:/var/lib/registry \
        -e REGISTRY_HTTP_ADDR=0.0.0.0:443 \
        -e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/hub.example.local.crt \
        -e REGISTRY_HTTP_TLS_KEY=/certs/hub.example.local.key \
        -p 443:443 registry:2
        ```

**Hello world với Docker**

![lỗi](./ImageDocker/Dock3.png)

**3. Các câu lệnh cơ bản**

- **Docker tag**
Tạo một thẻ TARGET_IMAGE tham chiếu đến SOURCE_IMAGE
```
$ docker tag SOURCE_IMAGE[:TAG] TARGET_IMAGE[:TAG]
```
- **Docker run**
Lệnh "docker run" thường được sử dụng cho một Docker container mới, tức là sử dụng trong trường hợp bạn chưa có container và bạn muốn tạo một container mới từ một file image, khởi động nó và chạy tiến trình được gán cho docker container đấy.
```
$ docker run [OPTIONS] IMAGE[:TAG|@DIGEST] [COMMAND] [ARG...]
```
- **Docker build**
Lệnh xây dựng các Docker Image từ Dockerfile và Context. Context của build là tập hợp các tệp nằm trong tệp được chỉ định. Quá trình build có thể tham chiếu đến bất kỳ tệp nào trong context.
```
    $ docker build [OPTIONS] PATH | URL | -
```
- **Docker push**
Push một image hoặc một repository vào regidtry
Dùng để chia sẻ Images đến Docker Hub registry hoặc với một Image self-hosted 
```
$ docker push [OPTIONS] NAME[:TAG]
```
- **Docker pull**
Pull một image hoặc một reoisitory ra từ registry.
Hầu hết images sẽ được tạo trên  một base image từ Docker Hub registry. Docker Hub chứa nhiều images được tạo sẵn mà bạn có thể pull và thử mà không cần phải xác định và configure. Để tải xuống Images, sử dụng công cụ Docker pull.
```
$ docker pull [OPTIONS] NAME[:TAG|@DIGEST]
```

**4. Một số kiến thức khác**
- **Docker có 2 phiên bản chính:**
    - **Docker EE (Docker Eterprise Edition)**
    Docker EE có 3 versions chính là Basic, Standard, Advanced. Bản Basic bao gồm Docker platform, hỗ trợ support và certification. Bản Standard và Advanced thêm các tính năng như container management (Docker Datacenter) và Docker Security Scanning.
    Docker EE được support bởi Alibaba, Canonical, HPE, IBM, Microsoft…
    Docker cũng cung cấp một certification để giúp các doanh nghiệp đảm bảo các sản phẩm của họ được hoạt động với Docker EE.

    ![lỗi](./ImageDocker/Dock4.png)
   
    - **Docker CE (Docker Community Edition)**
Docker CE là một phiên bản Docker do cộng đồng support và phát triển, hoàn toàn miễn phí.
Có hai phiên bản của Docker CE là Edge và Stable. Bản Edge sẽ được release hàng tháng với các tính năng mới nhất, còn Stable sẽ release theo quý.

        **Tóm lại** Docker CE là free còn EE thì là mất tiền . Cả hai phiên bản Docker CE vào Docker EE tuy hướng vào các đối tượng khác nhau nhưng chúng không có quá nhiều sự khác biệt. Cả hai đều dựa vào Docker open source được phát triển bởi cả cộng đồng và các đối tác của Docker.
- **Docker Engine**
Docker Engine là công cụ Client - Server hỗ trợ công nghệ container để xử lý các nhiệm vụ và quy trình công việc liên quan đến việc xây dựng các ứng dụng dựa trên vùng chứa (container). Engine tạo ra một quy trình daemon phía máy chủ lưu trữ images, containers, networks và storage volumes. Daemon cũng cung cấp giao diện dòng lệnh phía máy khách (CLI) cho phép người dùng tương tác với daemon thông qua giao diện lập trình ứng dụng Docker.

- **Docker Compose**
Docker compose là công cụ dùng để định nghĩa và run multi-container cho Docker application. Với compose bạn sử dụng file YAML để config các services cho application của bạn. Sau đó dùng command để create và run từ những config đó
Docker compose cho phép tạo nhiều service(container) giống nhau bằng lệnh:
    *$ docker-compose scale <tên service> = <số lượng>*

- **Docker Registry**
Docker Registry là một dịch vụ máy chủ cho phép lưu trữ các docker image của cá nhân, công ty, team,… Dịch vụ Docker Registry có thể được cung cấp bởi tổ chức thứ 3 hoặc là dịch vụ nội bộ được xây dựng riêng nếu bạn muốn. Một số dịch vụ Docker Registry phổ biến như :
    + Azure Container Registry
    + Docker Hub
    + Quay Enterprise
    + Google Container Registry.
    + AWS Container Registry

    ![lỗi](./ImageDocker/Dock5.png)

- **Docker Daemon**
Docker daemon (dockerd) nghe các yêu cầu từ Docker API và quản lý các đối tượng Docker như images, containers, network và volumn. Một daemon cũng có thể giao tiếp với các daemon khác để quản lý các Docker services.

- **Docker Network**

    Docker network sẽ đảm nhiệm nhiệm vụ kết nối mạng giữa các container với nhau, kết nối giữa container với bên ngoài, cũng như kết nối giữa các cụm (swarm) docker containers.

    Hệ thống network Docker là dạng plugable, sử dụng drivers. Hầu hết các driver được cung cấp mặc định, với các chức năng cốt lõi của các chức năng mạng thông thường. Docker network có thể cung cấp hầu hết các chức năng mà một hệ thống mạng bình thường cần có.
    - *Bridge*: Đây là driver mạng default của Docker. Nếu không chỉ định driver thì bridge sẽ là driver mạng mặc định khi khởi tạo. Bridge network thường được sử dụng khi cần chạy ứng dụng dưới dạng các container độc lập cần giao tiếp với nhau. Các container trong cùng mạng có thể giao tiếp với nhau qua địa chỉ IP. Docker không hỗ trợ nhận diện host ở mạng này, vì vậy muốn connect thì phải dùng options links để docker có thể hiểu được địa chỉ của các service.
    Bridge là driver tốt nhất cho việc giao tiếp multiple containers ở một host đơn.
    - *Host*: Dùng khi container cần giao tiếp với host và sử dụng luôn mạng ở host, vì sử dụng mạng của máy chủ đang chạy nên không còn lớp mạng nào giữa container với Docker Host phù hợp khi cần connect từ container ra thẳng ngoài host.
    - *Overlay*: Mạng lớp phủ - Overlay network tạo một mạng phân tán giữa nhiều máy chủ Docker. Kết nối nhiều Docker daemons với nhau và cho phép các cụm services giao tiếp với nhau. Có thể sử dụng overlay network để giao tiếp dễ dàng giữa cụm các services với một container độc lập, hay giữa 2 container với nhau ở khác máy chủ Docker daemons.
    - *MACVLAN*: Mạng Macvlan cho phép chúng ta gán địa chỉ MAC cho container, điều này làm cho mỗi container như là một thiết bị vật lý trong mạng. Docker daemon định tuyến truy cập tới container bởi địa chỉ MAC.
    - *None*: Với container không cần networking hoặc cần disable đi tất cả mọi networking, chúng ta sẽ chọn driver này. Thường được dùng với mạng tùy chỉnh. Driver này không thể dùng trong cụm swarm.

- **Docker Volume**
    
    Docker volume là một volume được tạo ra cho phép các container mount volume vào trong các container hay dễ hiểu hơn là docker sử dụng volume đó thay thế cho 1 folder của container.

    ![lỗi](./ImageDocker/Dock6.png)

    - Volumes giản hóa việc backup hoặc migrate hơn bind mount.
    - Bạn có thể quản lý volumes sử dụng các lệnh Docker CLI và Docker API.
    - Volumes làm việc được trên cả Linux và Windows container.
    - Volumes có thể an toàn hơn khi chia sẻ dữ liệu giữa nhiều container.
    - Volume drivers cho phép bạn lưu trữ volumes trên remote hosts or cloud providers, để mã hóa nội dung của volumes, hoặc thêm các chức năng khác.
    - Các nội dung của volume mới có thể được điền trước bởi một container.

**5. Nguồn tham khảo**

[Reference documentation | Docker Documentation](https://docs.docker.com/reference/)

[vietnix.vn/docker](https://vietnix.vn/docker-la-gi/#docker-trong-container-la-gi)

[Docker và những kiến thức cơ bản](https://viblo.asia/p/docker-va-nhung-kien-thuc-co-ban-YWOZrp075Q0)

[Tìm hiểu về Docker](https://blog.cloud365.vn/container/tim-hieu-docker-phan-2/)

[Docker Networking - Những khái niệm và cách sử dụng cơ bản](https://viblo.asia/p/docker-networking-nhung-khai-niem-va-cach-su-dung-co-ban-gGJ59P2JlX2)

[Private Docker Registry](https://vnsys.wordpress.com/2018/11/05/dung-private-docker-registry/)