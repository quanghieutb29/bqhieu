#### Tìm hiểu kiến thức về Markdown và Git

**I. Markdown**
**1. Markdown là gì?**

Markdown là ngôn ngữ đánh dấu văn bản đã có mặt từ rất lâu, với cú pháp đơn giản, dễ hiểu nên đã được nhiều người biết đến và được sử dụng khá phổ biến. Đây là loại ngôn ngữ đánh dấu văn bản được tạo ra vào năm 2004 bởi John Gruber.2

**2. Cú pháp cơ bản** 
- **Tiêu đề - Heading**

Để tạo tiêu đề - heading h1, h2, h3 cho đến h6, thêm số lượng ký tự # tương ứng vào đầu dòng. Số lượng # bạn sử dụng tương ứng với cấp độ tiêu đề, một ký tự # tương đương với h1, 2 ký tự # tương đương với h2...

- **Đoạn văn – Paragraph**

Để tạo các đoạn văn, sử dụng một dòng trống để tách các dòng văn bản, không nên thụt lề các đoạn bằng dấu cách hoặc tab.

- **Chữ in đậm – Bold**

Để in đậm văn bản, thêm hai dấu hoa thị (\*\*) hoặc dấu gạch dưới (\_\_) trước và sau một từ hoặc cụm từ. 

Để in đậm chữ cái nằm giữa một từ để nhấn mạnh, thêm hai dấu hoa thị (\*\*) trước và sau các chữ cái (không sử dụng space).

- **In nghiêng – Italic**

Để in nghiêng văn bản, thêm một dấu hoa thị hoặc gạch dưới trước và sau một từ hoặc cụm từ. Để in nghiêng chữ cái nằm giữa một từ để nhấn mạnh, thêm một dấu sao trước và sau các chữ cái (không sử dụng space).

- **In đậm và in nghiêng**

Để nhấn mạnh văn bản bằng chữ in đậm và in nghiêng cùng một lúc, thêm ba dấu hoa thị hoặc ba dấu gạch dưới trước và sau một từ hoặc cụm từ.

Hoặc sử dụng \*\*\_ ở đầu (\*\*\_ đầu và \_\*\*cuối câu) nếu muốn bôi đậm và in nghiêng câu đó.

- **Trích dẫn – Blockquote**
- Blockquote có nhiều đoạn:

Blockquote có thể chứa nhiều đoạn. Thêm > vào các dòng trống giữa các đoạn.

- Blockquote lồng nhau:

Blockquote có thể được lồng trong một Blockquote khác. Thêm dấu >> ở phía trước đoạn bạn muốn lồng.

- **Danh sách**
- Danh sách có thứ tự:

Để tạo danh sách có thứ tự, bạn chỉ cần thêm các các số theo sau là dấu chấm trước nội dung muốn tạo. Các số không nhất thiết phải theo thứ tự 1 2 3 4 lần lượt, nhưng bạn nên bắt đầu bằng số một.

- Danh sách không có thứ tự:

Để định đạng danh sách có các gạch đầu dòng trong Markdown, dùng kí tự dấu gạch ngang -, dấu hoa thị \* hoặc dấu cộng + và một dấu cách trước nội dung muốn tạo, dùng thêm 2 dấu cách ở đằng trước nếu muốn lùi vào một level.

- **Code**

Để biểu thị một từ hoặc cụm từ dưới dạng code, hãy đặt nó trong dấu  “ **`** ”.

- **Đường kẻ ngang – Horizontal rule**

Để tạo đường kẻ ngang, hãy sử dụng ba dấu sao \*\*\*, dấu gạch ngang --- hoặc dấu gạch dưới \_\_\_ trên cùng một dòng.

- **Liên kết – Link**

Một liên kết được tạo tự động với cặp móc nhọn < link > đơn giản bao quanh liên kết.

- **Hình ảnh**

Để thêm hình ảnh trong markdown, bạn thêm ký tự ! vào đầu tiên, sau đó ghi alt text trong ngoặc vuông [ ] và URL ảnh trong ngoặc đơn ( ).

\+ Chèn liên kết vào hình ảnh

  Đặt toàn bộ khai báo hình ảnh như bước trên trong ngoặc vuông [ ] và thêm liên kết mình cần vào ngoặc đơn ( ) đặt ngay tiếp sau.

**3. Tại sao phải dùng Markdown?**
- Markdown rất phổ biến, cách sử dụng cực kỳ đơn giản và tiện dụng, tối ưu hóa việc soạn thảo bằng HTML.
- Cú pháp thân thiện, không rắc rối giúp tiết kiệm thời gian.

**4. Convert Markdown ra HTML thế nào?**

Để chuyển đổi Markdown ra HTML ta sử dụng công cụ chuyển đổi

\+ Công cụ dòng lệnh: PowerShell, Pandoc, Showdown

\+ Sử dụng trình soạn thảo GUI: Notepad++,  Atom, Visual Studio Code, Typora, Dillinger...

\+ Công cụ khác: Web tools, Static site Generator
